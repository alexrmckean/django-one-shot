from django.shortcuts import redirect, render, get_object_or_404
from todos.models import TodoItem, TodoList
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.


def show_todolist(request):
  todo_list = TodoList.objects.all()
  context = {
    "todo_list": todo_list,
  }
  return render(request, "todos/list.html", context)


def show_todo_item(request, id):
  todo_item = TodoList.objects.get(id=id)
  context = {
    "todo_item": todo_item,
  }
  return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            items = form.save()
            return redirect("todo_list_detail", id=items.id)

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoListForm()

    context = {"form": form,}

    return render(request, "todos/create.html", context)


def update_todo_list(request, id):
  item = get_object_or_404(TodoList, id=id)
  if request.method == "POST":
    form = TodoListForm(request.POST, instance=item)
    if form.is_valid():
      # To redirect to the detail view of the model, use this:
      form.save()
      return redirect("todo_list_detail", id=id)

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
  else:
    form = TodoListForm(instance=item)

  context = {
    "todo_item": item,
    "form": form,
  }

  return render(request, "todos/edit.html", context)

def delete_todo_list(request, id):
  item = TodoList.objects.get(id=id)
  if request.method == "POST":
    item.delete()
    return redirect("todo_list_list")

  return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.id)

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoItemForm()

    context = {"form": form,}

    return render(request, "todos/itemcreate.html", context)
