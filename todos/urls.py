from django.urls import path
from todos.views import create_todo_item, delete_todo_list, show_todolist, show_todo_item, create_todo_list, update_todo_list

urlpatterns = [
    path("", show_todolist, name="todo_list_list"),
    path("<int:id>/", show_todo_item, name="todo_list_detail"),
    path("create/", create_todo_list, name="todo_list_create"),
    path("<int:id>/edit/", update_todo_list, name="todo_list_update"),
    path("<int:id>/delete/", delete_todo_list, name="todo_list_delete"),
    path("items/create/", create_todo_item, name="todo_item_create"),
]
